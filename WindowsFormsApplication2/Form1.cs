﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
   
    public partial class Form1 : Form
    {
        public class Point
        {
            public char x;
            public char y;
            public Point(char x, char y)
            {
                this.x = x;
                this.y = y;
            }
            public void swap()
            {
                char a = x;
                x = y;
                y = a;
            }
            public Boolean isIt(Point point)
            {
                return ((point.x == x) && (point.y == y));
            }
            public override string ToString()
            {
                return "("+x+","+y+")";
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox1.Text = openFileDialog1.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox2.Text = saveFileDialog1.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RunTest(readArr("1 2 1 3 4 2 2 3 3 3"));
        }
        public Point[] readArr(String s){
           String[] elements =s.Split(new char[]{' '});
           List<Point> points=new List<Point>();
           for (int i = 0,j=0; i < 5; i++,j=j+2)
               points.Add(new Point(elements[j][0],elements[j + 1][0]));
           return points;
        }
        public void RunTest(Point[] arr){
            Point[] arr1 = new Point[arr.Length];
            arr.CopyTo(arr1 ,0);
            textBox3.Text +="P="+toString(arr)+"\n";
            foreach (Point pnt in arr1)
                pnt.swap();
            textBox3.Text += "P-1=" + toString(arr1) + "\n";
            Point[] P0P=Komposition(arr, arr);
            textBox3.Text += "P0P=" + toString(P0P) + '\n';
        }
        public void unP(Point[] arr){
            foreach(Point a in arr)
                a.swap();
        }
        public Point[] Komposition(Point[] arr1, Point[] arr2)
        {
            List<Point> result=new List<Point>();
            foreach (Point pnt1 in arr1)
             foreach (Point pnt2 in arr2)
                    if (pnt1.y == pnt2.x)
                    {
                        Point newPoint = new Point(pnt1.x, pnt2.y);
                        if (!result.Contains(newPoint))
                            result.Add(newPoint);
                    }
            
            return result.ToArray();
        }
        public String toString(Point[] arr)
        {
            String res="{";
            foreach (Point a in arr)
                res += a.ToString() ;
            res += "}";
            
            return res;
        }
        
    }
}
